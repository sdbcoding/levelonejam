/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID ACCELERATE = 3089599978U;
        static const AkUniqueID AUTOSHOP01 = 1212036097U;
        static const AkUniqueID AUTOSHOP02 = 1212036098U;
        static const AkUniqueID AUTOSHOP03 = 1212036099U;
        static const AkUniqueID AUTOSHOPEND = 443547655U;
        static const AkUniqueID BROTHEL1 = 3504740484U;
        static const AkUniqueID BROTHEL2 = 3504740487U;
        static const AkUniqueID BROTHEL3 = 3504740486U;
        static const AkUniqueID CARCRASH = 4216331358U;
        static const AkUniqueID CARPASSING = 3026204914U;
        static const AkUniqueID ENGINESTART = 164866881U;
        static const AkUniqueID ENGINESTOP = 2079244539U;
        static const AkUniqueID PAUSERADIO = 107826350U;
        static const AkUniqueID PAUSETRUCKAMBIENCE = 2460650122U;
        static const AkUniqueID PAUSETRUCKERFEEDBACKLOOP = 1364393352U;
        static const AkUniqueID PLAY_AUTOSHOPSTART = 1288594709U;
        static const AkUniqueID PLAY_BIKER01 = 4113547392U;
        static const AkUniqueID PLAY_BIKER02 = 4113547395U;
        static const AkUniqueID PLAY_BIKER03 = 4113547394U;
        static const AkUniqueID PLAY_BIKEREND = 3623471800U;
        static const AkUniqueID PLAY_BIKERSTART = 1856143971U;
        static const AkUniqueID PLAY_BILLBOARD01 = 1084356854U;
        static const AkUniqueID PLAY_BILLBOARD02 = 1084356853U;
        static const AkUniqueID PLAY_BILLBOARD03 = 1084356852U;
        static const AkUniqueID PLAY_BILLBOARDEND = 2233950790U;
        static const AkUniqueID PLAY_BILLBOARDSTART = 106077997U;
        static const AkUniqueID PLAY_BROTHEL01 = 3127708007U;
        static const AkUniqueID PLAY_BROTHEL02 = 3127708004U;
        static const AkUniqueID PLAY_BROTHEL03 = 3127708005U;
        static const AkUniqueID PLAY_BROTHELEND = 2485707093U;
        static const AkUniqueID PLAY_BROTHELSTART = 970428702U;
        static const AkUniqueID PLAY_DUMPSITE01 = 394434668U;
        static const AkUniqueID PLAY_DUMPSITE02 = 394434671U;
        static const AkUniqueID PLAY_DUMPSITE03 = 394434670U;
        static const AkUniqueID PLAY_DUMPSITEEND = 319935972U;
        static const AkUniqueID PLAY_DUMPSITESTART = 1382336663U;
        static const AkUniqueID PLAY_FASTFOOD01 = 3417114433U;
        static const AkUniqueID PLAY_FASTFOOD02 = 3417114434U;
        static const AkUniqueID PLAY_FASTFOOD03 = 3417114435U;
        static const AkUniqueID PLAY_FASTFOODEND = 3232272455U;
        static const AkUniqueID PLAY_FASTFOODSTART = 1472800052U;
        static const AkUniqueID PLAY_GASSTATION01 = 1381963862U;
        static const AkUniqueID PLAY_GASSTATION02 = 1381963861U;
        static const AkUniqueID PLAY_GASSTATION03 = 1381963860U;
        static const AkUniqueID PLAY_GASSTATIONEND = 3521206566U;
        static const AkUniqueID PLAY_GASSTATIONSTART = 625733901U;
        static const AkUniqueID PLAY_HITCHHIKER01 = 3819494096U;
        static const AkUniqueID PLAY_HITCHHIKER02 = 3819494099U;
        static const AkUniqueID PLAY_HITCHHIKER03 = 3819494098U;
        static const AkUniqueID PLAY_HITCHHIKEREND = 4031716456U;
        static const AkUniqueID PLAY_HITCHHIKERSTART = 3936872851U;
        static const AkUniqueID PLAY_LIQUORSTORE01 = 1415972064U;
        static const AkUniqueID PLAY_LIQUORSTORE02 = 1415972067U;
        static const AkUniqueID PLAY_LIQUORSTORE03 = 1415972066U;
        static const AkUniqueID PLAY_LIQUORSTOREEND = 2590417944U;
        static const AkUniqueID PLAY_LIQUORSTORESTART = 3214869955U;
        static const AkUniqueID PLAY_MAGICFIRE01 = 3785305306U;
        static const AkUniqueID PLAY_MAGICFIRE02 = 3785305305U;
        static const AkUniqueID PLAY_MAGICFIRE03 = 3785305304U;
        static const AkUniqueID PLAY_MAGICFIREEND = 3492074090U;
        static const AkUniqueID PLAY_MAGICFIRESTART = 3123566937U;
        static const AkUniqueID PLAY_MEXICANEND = 1775693328U;
        static const AkUniqueID PLAY_MEXICANSTART = 2349312171U;
        static const AkUniqueID PLAY_RESTSTOP01 = 4219724149U;
        static const AkUniqueID PLAY_RESTSTOP02 = 4219724150U;
        static const AkUniqueID PLAY_RESTSTOP03 = 4219724151U;
        static const AkUniqueID PLAY_RESTSTOPEND = 1029295979U;
        static const AkUniqueID PLAY_RESTSTOPSTART = 1454516304U;
        static const AkUniqueID PLAY_SHOPPINGMALL01 = 4073760789U;
        static const AkUniqueID PLAY_SHOPPINGMALL02 = 4073760790U;
        static const AkUniqueID PLAY_SHOPPINGMALL03 = 4073760791U;
        static const AkUniqueID PLAY_SHOPPINGMALLEND = 2851737995U;
        static const AkUniqueID PLAY_SHOPPINGMALLSTART = 3400226160U;
        static const AkUniqueID PLAY_SPEEDCAM01 = 3241612321U;
        static const AkUniqueID PLAY_SPEEDCAM02 = 3241612322U;
        static const AkUniqueID PLAY_SPEEDCAM03 = 3241612323U;
        static const AkUniqueID PLAY_SPEEDCAMEND = 2855747495U;
        static const AkUniqueID PLAY_SPEEDCAMSTART = 1750448532U;
        static const AkUniqueID PLAY_TEQUILA01 = 2896398330U;
        static const AkUniqueID PLAY_TEQUILA02 = 2896398329U;
        static const AkUniqueID PLAY_TEQUILA03 = 2896398328U;
        static const AkUniqueID PLAY_TEQUILAEND = 2281615882U;
        static const AkUniqueID PLAY_TEQUILASTART = 2676681465U;
        static const AkUniqueID PLAY_TRUCKPARKING01 = 3130371464U;
        static const AkUniqueID PLAY_TRUCKPARKING02 = 3130371467U;
        static const AkUniqueID PLAY_TRUCKPARKING03 = 3130371466U;
        static const AkUniqueID PLAY_TRUCKPARKINGEND = 1869456000U;
        static const AkUniqueID PLAY_TRUCKPARKINGSTART = 804698715U;
        static const AkUniqueID PLAY_VEGGIESTAND01 = 2122396318U;
        static const AkUniqueID PLAY_VEGGIESTAND02 = 2122396317U;
        static const AkUniqueID PLAY_VEGGIESTAND03 = 2122396316U;
        static const AkUniqueID PLAY_VEGGIESTANDEND = 3817841646U;
        static const AkUniqueID PLAY_VEGGIESTANDSTART = 3470630149U;
        static const AkUniqueID PLAYCLICK = 2002511053U;
        static const AkUniqueID PLAYGASSTATION01 = 794605379U;
        static const AkUniqueID PLAYGASSTATION02 = 794605376U;
        static const AkUniqueID PLAYGASSTATION03 = 794605377U;
        static const AkUniqueID PLAYHITCHHIKER01 = 3126943301U;
        static const AkUniqueID PLAYHITCHHIKER02 = 3126943302U;
        static const AkUniqueID PLAYHITCHHIKER03 = 3126943303U;
        static const AkUniqueID PLAYMEXICAN01 = 1541792435U;
        static const AkUniqueID PLAYMEXICAN02 = 1541792432U;
        static const AkUniqueID PLAYMEXICAN03 = 1541792433U;
        static const AkUniqueID PLAYRADIOLOOP = 3528264838U;
        static const AkUniqueID PLAYTRUCKAMBIENCELOOP = 1750266586U;
        static const AkUniqueID PLAYTRUCKERFEEDBACKLOOP = 1429535606U;
        static const AkUniqueID RESUMERADIO = 1436376325U;
        static const AkUniqueID RESUMETRUCKAMBIENCE = 79087081U;
        static const AkUniqueID RESUMETRUCKERFEEDBACKLOOP = 996029715U;
        static const AkUniqueID STOPRADIO = 1269015798U;
        static const AkUniqueID TRUCKERMOOD1 = 3529574263U;
        static const AkUniqueID TRUCKERMOOD2 = 3529574260U;
        static const AkUniqueID TRUCKERMOOD3 = 3529574261U;
        static const AkUniqueID TRUCKERMOOD4 = 3529574258U;
        static const AkUniqueID TRUCKERMOOD5 = 3529574259U;
        static const AkUniqueID VEGETABLESTAND1 = 1145206703U;
        static const AkUniqueID VEGETABLESTAND2 = 1145206700U;
        static const AkUniqueID VEGETABLESTAND3 = 1145206701U;
    } // namespace EVENTS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID GAME = 702482391U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID AMBIENCE = 85412153U;
        static const AkUniqueID DIALOG = 1235749641U;
        static const AkUniqueID FEEDBACK = 4228153068U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MUSICA = 1730564739U;
        static const AkUniqueID PLACEHOLDER = 1548734028U;
        static const AkUniqueID UI = 1551306167U;
    } // namespace BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
