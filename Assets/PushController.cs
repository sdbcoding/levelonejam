﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushController : MonoBehaviour
{
    public Vector3 PushForce = new Vector3(50f, 0f, 50f);
    public GameObjectVariable hitObject;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision) 
    {
        if( collision.gameObject.layer == 11 || collision.gameObject.layer == 13)
        {
            Debug.Log("Car collided");

            // Calculate Angle Between the collision point and the player
            Vector3 dir = collision.contacts[0].point - transform.position;
            // We then get the opposite (-Vector3) and normalize it
            dir = -dir.normalized;
            Rigidbody rb = collision.gameObject.GetComponent<Rigidbody>();
            if (rb == null)
            {
                rb = collision.gameObject.AddComponent<Rigidbody>();
            }
            rb.AddForce(new Vector3(Random.Range(0,PushForce.x), Random.Range(0,PushForce.y),Random.Range(0,PushForce.z)), ForceMode.Impulse);
            foreach (BoxCollider item in collision.gameObject.GetComponents<BoxCollider>())
            {
                item.enabled = false;
            }
			AkSoundEngine.PostEvent("CarCrash", gameObject);
            hitObject.SetValue(collision.gameObject);
        }    
    }
}
