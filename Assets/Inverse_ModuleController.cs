﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inverse_ModuleController : OnUpdate
{
    // Inputs
    public GameObject Moving;
    public GameObjectVariable ToSpawnAfterModule;
    public GameObjectVariable ToRemoveModule;
    private List<GameObject> ActiveList = new List<GameObject>();
    public GameObject Pool;
    private List<GameObject> PoolList = new List<GameObject>();
    public GameObject[] prefabObjects;

    // Module movement
    public float MovementSpeed = 10;
    private Vector3 MovementVector;

    // Spawning
    private int previousIndex, nextIndex = 0;
    private Vector3 SpawnPosition;
    
    // Decisions
    public GameObject DecisionModule;
    public float TimeToDecide = 4f;
    private bool DecisionIncoming = false;
    private float DecisionTime = 0f;
    public bool DecisionTaken = false;

    // Start is called before the first frame update
    void Start()
    {
        GameObject[] teps = GameObject.FindGameObjectsWithTag("SpawnTrigger");
        Debug.Log("Size: " + teps.Length);
        MovementVector = new Vector3(0f,0f,-MovementSpeed);
        SpawnPosition = new Vector3(20.4f, -11.407f, 155.86f);

        Transform[] children = Moving.GetComponentsInChildren<Transform>();
        foreach (Transform child in children)
        {
            if (child.parent == Moving.transform)
            {
                ActiveList.Add(child.gameObject);
            }
        }

        Transform[] children2 = Pool.GetComponentsInChildren<Transform>();
        foreach (Transform child in children2)
        {
            if (child.parent == Pool.transform)
            {
                PoolList.Add(child.gameObject);
                child.gameObject.SetActive(false);
            }
        }
        Debug.Log("Pool size: " + PoolList.Count);
    }

    // Update is called once per frame
    public override void UpdateEventRaised(float deltaTime)
    {
        foreach (GameObject t in ActiveList)
        {
            if( t.tag == "Module" )
            {
                t.transform.Translate(MovementVector * deltaTime);
            }
        }

        if( DecisionTime > 0f )
        {
            DecisionTime += deltaTime;
            if( DecisionTime > TimeToDecide-2.7f )
            {
                DecisionIncoming = true;
            }
            Debug.Log("Decision time: "+ DecisionTime);
        }

        if( DecisionTaken )
        {
            DecisionTime = 0f;
            DecisionTaken = false;
        }
        // MovingModules.transform.Translate(MovementVector);   
    }

    public void SpawnNewModule()
    {
        Debug.Log("Spawnin: " + Time.fixedUnscaledTime + " too: " + ToSpawnAfterModule.Value.name);
        if (ToSpawnAfterModule.Value.name == "Module1")
        {
            Debug.Log("BAD OUTCOME");
            return;
        }
        GameObject nextModule;
        if( !DecisionIncoming )
        {
            // Debug.Log("A wild position appeared!");
            previousIndex = nextIndex;
            // Get random index
            int count = 0;
            bool broken = false;
            while( previousIndex == nextIndex )
            {
                nextIndex = Random.Range( 0, PoolList.Count);
                count++;
                if (count == 10)
                {
                    broken = true;
                    break;
                }
            }
            //Debug.Log("Selected to spawn: " + nextIndex);

            if (broken)
            {
                nextModule = Instantiate(prefabObjects[Random.Range(0, prefabObjects.Length)]);
            } else
            {
                nextModule = PoolList[nextIndex];
            }

        }
        else
        {
            Debug.Log("DecisionIncoming ");
            nextModule = DecisionModule;
            DecisionIncoming = false;
        }

        // Get correct position to spawn
        //Debug.Log("Spawning: " + nextModule.name);
        SpawnPosition = ToSpawnAfterModule.Value.GetComponent<ModuleRef>().GetSpawnPoint().transform.position;
        // SpawnPosition.z = -20.8f;

        //Debug.Log("Inverse ModuleController is called to spawn shit");
        nextModule.transform.SetParent(transform);
        Debug.Log("Pre: " + SpawnPosition +" to module: " + nextModule.name);
        nextModule.transform.Translate( -(SpawnPosition - nextModule.transform.position ) );
        nextModule.transform.Translate( - new Vector3(0f,0,34f) );            
     
        // Add/remove from lists
        nextModule.SetActive(true);
        PoolList.Remove(nextModule);  
        ActiveList.Add(nextModule);      
    }

    public void RemoveModule( )
    {
        ActiveList.Remove(ToRemoveModule.Value);
        if (ToRemoveModule.Value.name == "Module1")
        { return; }
        // ToRemoveModule.Value.transform.parent = ModulePool.transform;
        PoolList.Add(ToRemoveModule.Value);
        ToRemoveModule.Value.SetActive(false);
    }

    public void SetDecisionIncoming()
    {
        DecisionIncoming = true;
        DecisionTime = 0.0000001f;
    }
    
}
