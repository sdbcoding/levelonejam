﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DecisionAreaController : MonoBehaviour
{
    public BooleanVariable StopDecision;
    public GameEvent DecisionIgnored;
    public StopController StopController;
    public ModuleController ModuleController;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other) 
    {
        if( other.gameObject.tag == "DecisionTriggerArea" )
        {
            ModuleController.DecisionTaken = true;
            if( StopDecision.Value )
            {
                StopController.Stop();
                StopDecision.SetValue(false);
            }
            else
            {
                // Continue
                DecisionIgnored.Raise();
            }
        }
    }
}
