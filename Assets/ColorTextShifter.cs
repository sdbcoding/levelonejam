﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorTextShifter : ColorAlphaChanger
{
    public Text text; 
    // Start is called before the first frame update
    void Start()
    {
        text.color = new Color(text.color.r, text.color.g, text.color.b, 0f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void FixedUpdate() 
    {
        ShiftColor();
    }

    public virtual void StartShift(float alpha)
    {
        nextColor = new Color(text.color.r, text.color.g, text.color.b, alpha/255f);
        shift = true;
    }

    public virtual void ShiftColor()
    {
        if( shift )
        {
            text.color = Color.Lerp( text.color, nextColor, Time.time );
        }
    }
}
