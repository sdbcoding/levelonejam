﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpDownBouncer : MonoBehaviour
{
    public float bounceDistance;
    private Vector3 bounceFrom;
    private Vector3 bounceTo;
    public bool bouncing = false;
    // Start is called before the first frame update
    void Start()
    {
        bounceFrom = gameObject.transform.localPosition;
        bounceTo = new Vector3(gameObject.transform.localPosition.x, gameObject.transform.localPosition.y - bounceDistance, gameObject.transform.localPosition.z);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if( bouncing )
        {
            gameObject.transform.localPosition = Vector3.Lerp(bounceFrom, bounceTo, Time.deltaTime*2f);
            if( Vector3.Distance( bounceFrom, bounceTo) <= 0.01f ) bouncing = false;
        }
        else
        {
            gameObject.transform.localPosition = Vector3.Lerp(bounceTo, bounceFrom, Time.deltaTime*2f);  
            if( Vector3.Distance( bounceTo, bounceFrom) <= 0.01f ) bouncing = true;
        }
    }

    public void Bounce()
    {
        bouncing = true;
    }
}
