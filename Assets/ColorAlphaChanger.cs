﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorAlphaChanger : MonoBehaviour
{
    protected bool shift = false;
    protected Color nextColor;
    public Image image;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void FixedUpdate() 
    {
        ShiftColor();
    }

    public virtual void StartShift(float alpha)
    {
        nextColor = new Color(image.color.r, image.color.g, image.color.b, alpha/255f);
        shift = true;
    }

    public virtual void ShiftColor()
    {
        if( shift )
        {
            image.color = Color.Lerp( image.color, nextColor, Time.time );
        }
    }
}
