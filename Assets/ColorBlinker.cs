﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorBlinker : ColorAlphaChanger
{
    public Color originalColor;
    // Start is called before the first frame update
    void Start()
    {
        originalColor = image.color;
        image.color = new Color(image.color.r, image.color.g, image.color.b, 0f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void FixedUpdate() 
    {
        ShiftColor();
    }

    public virtual void ShiftColor()
    {
        if( shift )
        {
            image.color = Color.Lerp( image.color, nextColor, Time.deltaTime*2f);
            if( Mathf.Abs(nextColor.a - image.color.a) <= 0.1f ){ 
                shift = false;
                Debug.Log("Shift");
            }
        }
        else
        {
            image.color = Color.Lerp( image.color, originalColor, Time.deltaTime*2f);
            if( Mathf.Abs( image.color.a - originalColor.a) <= 0.1f ) shift = true;
            Debug.Log("Shifting opposite");
        }
    }
}
