﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SOReference", menuName = "ScriptableObject/SOReference", order = 0)]
public class SOReference : ScriptableObject 
{
	[SerializeField]
	GameObject reference;

	public GameObject GetReference()
	{
		return reference;
	}

	public void SetReference(GameObject go)
	{
		reference = go;
	}
}
