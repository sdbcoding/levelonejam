﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarPassingController : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Got one: " + other.name + " and " + other.gameObject.layer);
        if (other.gameObject.layer == LayerMask.NameToLayer("Car"))
        {
            AkSoundEngine.PostEvent("CarPassing", gameObject);
        }
    }
}
