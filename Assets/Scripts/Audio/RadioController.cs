﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadioController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        //AkSoundEngine.PostEvent("PlayRadioLoop", gameObject);

    }

    
    public void PlayRadio()
    {
        AkSoundEngine.PostEvent("ResumeRadioLoop", gameObject);
    }
    public void PauseRadio()
    {
        AkSoundEngine.PostEvent("PauseRadio", gameObject);

    }
}
