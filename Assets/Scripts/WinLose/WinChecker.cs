﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class WinChecker : MonoBehaviour
{
    public IntVariable decisionsMade;
    public int requiredNumberForWin = 6;
    public UnityEvent winGame;
    public void CheckDEcisionsMade()
    {
        if (decisionsMade.GetValue() >= requiredNumberForWin)
        {
            winGame.Invoke();
        }
    }
}
