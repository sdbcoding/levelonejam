﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class IntVaribleZeroChecker : GameEventListener
{
    [Header("IGNORE EVERYTHING ABOVE, Will sign up to event itself")]

    public IntVariable varibleToCheck;
    public UnityEvent EventIfBelow;
    private void Start()
    {
        varibleToCheck.RegisterListener(this);
        Response.AddListener(ChangeInVariable);
    }

    public void ChangeInVariable()
    {
        if (varibleToCheck.GetValue() <= 0)
        {
            EventIfBelow.Invoke();
        }
    }
}
