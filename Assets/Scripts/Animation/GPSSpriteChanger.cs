﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GPSSpriteChanger : MonoBehaviour
{
    public Sprite defaultSprite;
    public DecisionVariable upcomingDecision;

    public Image image;
    Color invisible;
    Color visible;

    private void Start()
    {
        invisible = new Color(1.0f, 1.0f, 1.0f, 0.0f);
        visible = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        image.color = invisible;
    }
    public void ChangeGPSSprite()
    {
        image.sprite = upcomingDecision.Value.sprite;
        image.color = visible;

    }
    public void DecisionIgnoreOrChosen()
    {
        image.sprite = defaultSprite;
        if (defaultSprite == null)
        {
            image.color = invisible;
        }
    }
}
