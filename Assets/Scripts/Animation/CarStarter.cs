﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarStarter : OnUpdate
{
    public Transform[] objectsToMove;
    Vector3[] orgPosition;

    public int minShake = 10;
    public int maxShake = 30;
    public float minTimeBetweenShake = 4;
    public float maxTimeBetweenShake = 10;
    public float minOffset = 5;
    public float maxOffset = 12;

    public int carStartShake = 25;
    int timesToRun = 100;
    int timesRun;
    float nextOffset = 10;

    public float currentTime;
    public float timeUntilNext;

    bool isShaking = false;

    private void Start()
    {
        orgPosition = new Vector3[objectsToMove.Length];
        for (int i = 0; i < objectsToMove.Length; i++)
        {
            if (objectsToMove[i] != null)
            {
                orgPosition[i] = objectsToMove[i].position;
            }
        }
        SetupShake();
        //StartCar();
    }


    public override void UpdateEventRaised(float deltaTime)
    {
        if (isShaking) { return; }
        currentTime += deltaTime;
        if (currentTime > timeUntilNext)
        {
            ActivateShake();
        }
    }

    public void StartCar()
    {
        timesToRun = carStartShake;
        ActivateShake();
    }

    public void ActivateShake()
    {
        isShaking = true;
        timesRun = 0;
        InvokeRepeating("ShakeCar", 0.2f, 0.03f);
    }

    void SetupShake()
    {
        timesToRun = Random.Range(minShake, maxShake);
        currentTime = 0;
        timeUntilNext = Random.Range(minTimeBetweenShake, maxTimeBetweenShake);
        nextOffset = Random.Range(minOffset, maxOffset);
    }


    public void ShakeCar()
    {
        Vector3 newPosition;
        for (int i = 0; i < objectsToMove.Length; i++)
        {
            if (objectsToMove[i] != null)
            {
                newPosition = orgPosition[i];
                newPosition.y += nextOffset;
                objectsToMove[i].position = newPosition;
            }
        }
        timesRun++;
        nextOffset *= -1;
        if (timesRun >= timesToRun)
        {
            CancelInvoke("ShakeCar");
            for (int i = 0; i < objectsToMove.Length; i++)
            {
                if (objectsToMove[i] != null)
                {
                    objectsToMove[i].position = orgPosition[i];
                }
            }
            isShaking = false;
            SetupShake();
        }
    }
}
