﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StaticScreen : MonoBehaviour
{
    public Sprite[] staticSprites;
    public Image staticImage;

    float nextEvent;
    float currentTime;

    public float maxVisible = 0.5f;
    public float minVisible = 0.2f;

    public float maxHidden = 4.0f;
    public float minHidden = 0.5f;
    
    // Update is called once per frame
    void Update()
    {
        currentTime += Time.deltaTime;
        if (currentTime > nextEvent)
        {
            currentTime = 0;
            if (staticImage.enabled)
            {
                staticImage.enabled = false;
                nextEvent = Random.Range(minHidden, maxHidden);
            } else
            {
                staticImage.enabled = true;
                if (staticSprites.Length != 0)
                {
                    staticImage.sprite = staticSprites[Random.Range(0, staticSprites.Length)];
                }
                nextEvent = Random.Range(minVisible, maxVisible);

            }
        }
    }
}
