﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandWithMoneyController : MonoBehaviour
{
    public RectTransform rectTransform;
    float exponent = 1.05f;
    float sign = 1;
    bool changedFLow = false;
    int ticks;
    Vector2 orgSize;
    Vector2 orgPos;
    // Start is called before the first frame update
    void Start()
    {
        orgSize = rectTransform.sizeDelta;
        orgPos = rectTransform.position;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 size = rectTransform.localScale;
        size += sign * ((size * exponent) - size);
        rectTransform.localScale = size;

        Vector3 newPosition = rectTransform.position;
        newPosition.x += -sign * ((newPosition.x * exponent) - newPosition.x);
        newPosition.y += sign * ((newPosition.y * exponent) - newPosition.y);

        rectTransform.position = newPosition;

        ticks++;
        if (ticks > 5)
        {
            sign *= -1;
            ticks = 0;
            if (changedFLow)
            {
                rectTransform.sizeDelta = orgSize;
                rectTransform.position = orgPos;
                Destroy(this);
            }else
            {
                changedFLow = true;

            }
        }
    }
}
