﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteeringWheelRotation : MonoBehaviour
{

    // Update is called once per frame
    void Update()
    {
        float newValue = Input.GetAxis("Horizontal");
        //Quaternion newRotation = new Quaternion(0, 0, newValue, 0);
        Quaternion newRotation = Quaternion.Euler(0.0f, 0.0f, -(Mathf.Rad2Deg* newValue));
        //Debug.Log(newValue);
        //Debug.Log(newRotation);
        GetComponent<RectTransform>().rotation = newRotation;
    }
}
