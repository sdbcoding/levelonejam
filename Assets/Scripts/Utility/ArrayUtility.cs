﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ArrayUtility {
	private static System.Random rng = new System.Random(); 

	public static void Shuffle<T>(this T[] list)  
	{  
		int n = list.Length;  
		while (n > 1) 
		{  
			n--;  
			int k = rng.Next(n + 1);  
			T value = list[k];  
			list[k] = list[n];  
			list[n] = value;  
		}
    }  
}
