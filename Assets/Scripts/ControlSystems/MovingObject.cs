﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingObject : OnUpdate
{
    Vector3 direction;
    float speed = 10;
    Quaternion startRotation;

    public float Speed
    {
        get
        {
            return speed;
        }

        set
        {
            speed = value;
        }
    }

    public Vector3 Direction
    {
        get
        {
            return direction;
        }

        set
        {
            direction = value;
        }
    }

    private void Start()
    {
        startRotation = transform.rotation;
    }

    public override void UpdateEventRaised(float deltaTime)
    {
        base.UpdateEventRaised(deltaTime);
        transform.position += direction * speed * deltaTime;
    }
    
    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log(gameObject.name + " <- I vs obj -> " + other.name);
        if (other.name == "BackPlane")
        {
            StopSelf();

        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.name == "WorldEdge")
        {
            StopSelf();
        }
    }

    void StopSelf()
    {
        gameObject.SetActive(false);
        transform.rotation = startRotation;
        Rigidbody rb = GetComponent<Rigidbody>();
        if (rb != null)
        {
            Destroy(rb);
        }
    }
}
