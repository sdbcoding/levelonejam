﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DecisionInputController : OnUpdate
{
    public float decisionTime = 3;
    public GameEvent decisionIgnored;
    public BooleanVariable decisionChosen;
    public GameEvent driveOver;
    float currentTime;

    bool haveIncomingDecision = false;
    public override void UpdateEventRaised(float deltaTime)
    {
        if (!haveIncomingDecision) { return; }
        
        currentTime += deltaTime;
        if (currentTime > decisionTime)
        {
            if (decisionChosen.Value)
            {
                driveOver.Raise();
            } else
            {
                decisionIgnored.Raise();
            }
            haveIncomingDecision = false;
            decisionChosen.SetValue(false);
        }
    }

    public void IncomingDecision()
    {
        haveIncomingDecision = true;
        currentTime = 0;
    }
}
