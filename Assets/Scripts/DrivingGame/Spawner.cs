﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObjectVariable SpawnEvent;
    public GameObjectVariable RemoveEvent;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other) 
    {
        // Debug.Log("The other tag: " + other.tag);
        if( other.tag == "SpawnTrigger" )
        {
            Debug.Log("Spawn triggered for module: " + gameObject.transform.parent.gameObject.name);
            // Debug.Break();
            SpawnEvent.SetValue(gameObject.transform.parent.gameObject);
        }

        if( other.tag == "RemoveTrigger" )
        {
            // Debug.Log("Remove triggered");
            RemoveEvent.SetValue(gameObject.transform.parent.gameObject);
        }
    }
}
