﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanePositionController : OnUpdate
{
    
    // Lane positions
    [SerializeField]
    GameObject Lanes;
    private List<Transform> SocketsArray = new List<Transform>();
    private int SocketPosition = 0;
    
    // Translation
    private bool Move = false;
    private bool Crashed = false;
    public float MovementSpeed = 10;

    // Rotation
    public float RotationSpeed = 1;
    private Quaternion OriginalRotation;
    private float currentLerpTime = 0f;
    public Vector3 RotationRight = new Vector3(0f, 45f, 0f);
    public Vector3 RotationLeft = new Vector3(0f, -45f, 0f);
    private bool RotateRight = false;
    private bool RotateLeft = false;
    private bool Reposition = false;

    // Start is called before the first frame update
    void Start()
    {
        foreach (Transform t in Lanes.transform )
        {
            SocketsArray.Add(t);
        }
        // Start at the right lane
        SocketPosition = SocketsArray.Count-1;
        transform.position = SocketsArray[SocketPosition].position;
        OriginalRotation = transform.rotation;
    }

    // Update is called once per frame
    public override void UpdateEventRaised(float deltaTime)
    {
        if( Input.GetButtonDown("Horizontal") )
        {
            currentLerpTime = 0f;
            Reposition = false;
            if( Input.GetAxis("Horizontal") > 0 )
            {
                Debug.Log("Right");
                // if( SocketPosition+1 < Sockets.length )
                if( SocketPosition+1 <= SocketsArray.Count-1 )
                {
                    SocketPosition += 1;
                    // Move to next socket
                    Move = true;
                    RotateLeft = false;
                    RotateRight = true;
                    currentLerpTime = 0f;
                }
                else
                {
                    // Crash 
                    Crashed = true;
                }
            }
            else if( Input.GetAxis("Horizontal") < 0  )
            {
                Debug.Log("Left");
                if( SocketPosition-1 >= 0 )
                {
                    SocketPosition -= 1;
                    // Move to next socket
                    Move = true;
                    RotateLeft = true;
                    RotateRight = false;
                    currentLerpTime = 0f;
                }
                else
                {
                    // Crash 
                    Crashed = false;
                }
            }
            else // Input == 0
            {

            }

        }

        //increment timer once per frame
        currentLerpTime += deltaTime;
        float tMovement = currentLerpTime / MovementSpeed; 
        float tRotation = currentLerpTime / RotationSpeed; 

        // Arrived to next socket
        if( Vector3.Distance(transform.position, SocketsArray[SocketPosition].position) <= 1f )
        {
            Debug.Log("Socket reached");
            Move = false;
            RotateLeft = false;
            RotateRight = false;
            Reposition = true;
        }

        if( Move )
        {
            // Quadratic time
           transform.position = Vector3.Lerp(transform.position, SocketsArray[SocketPosition].position, tMovement*tMovement);
        }
        if( RotateRight )
        {
            // Ease out time in lerp
            transform.rotation = Quaternion.Lerp( transform.rotation, Quaternion.Euler(RotationRight), Mathf.Sin(tRotation * Mathf.PI * 0.5f));
        }
        if( RotateLeft )
        {
            transform.rotation = Quaternion.Lerp( transform.rotation, Quaternion.Euler(RotationLeft), Mathf.Sin(tRotation * Mathf.PI * 0.5f));
        }
        if( Reposition )
        {   
            Debug.Log("Set original rotation");
            transform.rotation = Quaternion.Lerp( transform.rotation, OriginalRotation, deltaTime * RotationSpeed * 10 );
        }
        
        if( Crashed )
        {
            Debug.Log("You have fucked up!");
        }
    }

    private void SetOriginalRotation(float t)
    {
        Debug.Log("Set original rotation");
        // transform.rotation = OriginalRotation;
        transform.rotation = Quaternion.Lerp( transform.rotation, OriginalRotation, Mathf.Sin(t * Mathf.PI * 0.5f) );
    }
}
