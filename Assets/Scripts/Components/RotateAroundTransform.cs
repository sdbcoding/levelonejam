﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateAroundTransform : MonoBehaviour
{
    public Transform rotateTransform;
    public Vector3Variable directionAroundtransform;
    Vector3 newPosition;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        newPosition = directionAroundtransform.Value;
        newPosition.z = newPosition.y;
        newPosition.y = 0.0f;
        transform.up = newPosition;

        transform.position = newPosition + rotateTransform.position;
    }
}
