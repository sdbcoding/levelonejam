﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class LoadSceneFromScript : MonoBehaviour {

	public string sceneToLoad;
#if UNITY_EDITOR
    public UnityEditor.SceneAsset scene;

    void OnValidate()
    {

        sceneToLoad = UnityEditor.AssetDatabase.GetAssetPath(scene);
    }
#endif

    public void LoadScene()
	{
		SceneManager.LoadScene( sceneToLoad, LoadSceneMode.Single);
	}
}
