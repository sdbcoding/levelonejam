﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class DoubleTapToSkip : MonoBehaviour {

	public Text doubleTapText;
	float flashSpeed = 0.02f;
	public Color doubleTapTextColor; bool alphaUpwards = true; float alphaValue;
	bool doubleTapped = false;
	public UnityEvent response;

	// Use this for initialization
	void Start () {
		doubleTapTextColor = doubleTapText.color;
		alphaValue = doubleTapTextColor.a;
		
	}
	
	// Update is called once per frame
	void Update () {
		
		if (Input.touchCount != 0)
		{	
			if (Input.GetTouch(0).phase == TouchPhase.Ended)
			{
				if (doubleTapped)
				{
					CancelInvoke("HideText");
					CancelInvoke("ResetTime");
					response.Invoke();
					return;
				}
			doubleTapped = true;
			doubleTapText.gameObject.SetActive(true);
			Invoke("ResetTime",1);
			CancelInvoke("HideText");
			Invoke("HideText",3);
			}
		}



		UpdateTextAlpha();

	}
	void ResetTime()
	{
		doubleTapped = false;
	}
	void HideText()
	{
		doubleTapText.gameObject.SetActive(false);
	}

	private static bool IsDoubleTap()
	{
		bool result = false;
		float MaxTimeWait = 1;
		if( Input.touchCount == 1  && Input.GetTouch(0).phase == TouchPhase.Began)
		{
			float DeltaTime = Input.GetTouch (0).deltaTime;
			float DeltaPositionLenght=Input.GetTouch (0).deltaPosition.magnitude;
			Debug.Log("DeltaTime: " + DeltaTime);
			if ( DeltaTime> 0 && DeltaTime < MaxTimeWait )
				result = true;                
		}
		return result;
     }

	private void UpdateTextAlpha()
	{
		if( alphaValue <= 0f ){alphaUpwards = true;}
		else if( alphaValue >= 1f) {alphaUpwards = false;}

		alphaValue = alphaUpwards ? alphaValue+flashSpeed : alphaValue-flashSpeed;
		
		doubleTapTextColor.a = alphaValue;
		doubleTapText.color = doubleTapTextColor;
	}
}
