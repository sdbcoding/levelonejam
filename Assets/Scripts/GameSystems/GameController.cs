﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : OnUpdate
{
    public IntVariable decisionsMade;
    public int maxDecisions;
    public float minDistanceNeededBeforeNextDecision;
    public float maxDistanceNeededBeforeNextDecision;
    public GameEvent gameWon;
    public GameEvent findNewDecision;
    bool isRunning = true;

    float currentDistance;
    float nextDecisionTime;
    public void Pause()
    {
        isRunning = false;
    }
   
    private void Start()
    {
        decisionsMade.SetValue(0);
        FindNextTime();
    }
    void FindNextTime()
    {
        nextDecisionTime = Random.Range(minDistanceNeededBeforeNextDecision, maxDistanceNeededBeforeNextDecision);
    }
    public void MadeDecision()
    {
        decisionsMade.ApplyChange(1);
        if (decisionsMade.GetValue() == maxDecisions)
        {
            gameWon.Raise();
        } else
        {
            ResetTime();
        }
    }

    public void ResetTime()
    {
        currentDistance = 0;
        isRunning = true;
        FindNextTime();
    }

    public override void UpdateEventRaised(float deltaTime)
    {
        if (!isRunning) { return; }
        base.UpdateEventRaised(deltaTime);
        currentDistance += deltaTime;
        if (currentDistance> nextDecisionTime)
        {
            findNewDecision.Raise();
            currentDistance = 0.0f;
        }
    }
}
