﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(IntVariable))]

public class IntVariableEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        GUI.enabled = Application.isPlaying;

        IntVariable f = target as IntVariable;
        if (GUILayout.Button("Raise"))
            f.Raise();
    }
}
