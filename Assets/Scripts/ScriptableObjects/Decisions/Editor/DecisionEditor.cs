﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Decision))]

public class DecisionEditor : Editor
{
    public override void OnInspectorGUI()
    {

        base.OnInspectorGUI();
        Decision decision = target as Decision;
        if (GUILayout.Button("Update Number"))
            decision.SetupAllArrays();
        for (int i = 0; i < decision.responses.Length; i++)
        {
            ShowResponse(i,decision);
        }
    }

    void ShowResponse(int index, Decision decision)
    {
        EditorGUILayout.LabelField("Action: " + index.ToString());
        decision.responses[index] = (string)EditorGUILayout.TextField(new GUIContent().text = "Text", decision.responses[index]);
        decision.audio[index] = (string)EditorGUILayout.TextField(new GUIContent().text = "Audio", decision.audio[index]);
        decision.fucks[index] = (int)EditorGUILayout.IntField(new GUIContent().text = "Fucks", decision.fucks[index]);
        decision.truck[index] = (int)EditorGUILayout.IntField(new GUIContent().text = "Truck", decision.truck[index]);

    }
}
