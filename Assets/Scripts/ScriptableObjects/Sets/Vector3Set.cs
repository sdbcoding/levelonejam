﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]


public class Vector3Set : ScriptableObject {

	public List<Vector3> path = new List<Vector3>();

}
