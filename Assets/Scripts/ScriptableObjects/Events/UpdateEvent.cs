﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName ="UpdateLoop", menuName = "Events/UpdateEvent", order = 1)]


public class UpdateEvent : ScriptableObject {

	 #if UNITY_EDITOR
    [Multiline]
    public string DeveloperDescription = "";
    #endif
    private  List<OnUpdate> listeners = new List<OnUpdate>();
    private List<OnUpdate> deleteList = new List<OnUpdate>();

    void OnEnable()
    {
        listeners = new List<OnUpdate>();
        deleteList = new List<OnUpdate>();

    }
    public virtual void Raise(float deltaTime)
    {
        CheckListenersForNull();
        if (deleteList.Count != 0)
        {
            RemoveDeletedFromListeners();
        }
        for (int i = listeners.Count -1; i >= 0; i--)
        {
            listeners[i].UpdateEventRaised(deltaTime);
        }
        RemoveDeletedFromListeners();
        
        
    }
    void RemoveDeletedFromListeners()
    {
        for (int i = deleteList.Count - 1; i >= 0; i--)
        {
            listeners.Remove(deleteList[i]);
            deleteList.RemoveAt(i);
        }
    }
    public void ClearListeners()
    {
        listeners = new List<OnUpdate>();
        deleteList = new List<OnUpdate>();

    }

    public void RegisterListener(OnUpdate listener)
    {
        if (!listeners.Contains(listener))
        {
            listeners.Add(listener);
        } else {
            if (deleteList.Contains(listener))
            {
                deleteList.Remove(listener);
            }
        }
    }
    public void UnRegisterListener(OnUpdate listener)
    {
        if (listeners.Contains(listener))
        {
            //listeners.Remove(listener);
            deleteList.Add(listener);
        }
    }
    void CheckListenersForNull()
    {
        for (int i = listeners.Count - 1; i >=0 ; i--)
        {
            if (listeners[i] == null)
            {
                listeners.RemoveAt(i);
            }
        }
    }
    public int GetListenerSize()
    {
        return listeners.Count;
    }
}
