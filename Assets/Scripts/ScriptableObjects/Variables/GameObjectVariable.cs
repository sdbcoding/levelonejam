﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="Variables", menuName = "Variables/GameObject Variable", order = 3)]
public class GameObjectVariable : GameEvent 
{
	public GameObject Value;
        


    public void SetValue(GameObject value)
    {
        Value = value;
        Raise();

    }

    public void SetValue(GameObjectVariable value)
    {
        Value = value.Value;
        Raise();

    }
    public void ClearValue()
    {
        Value = null;
        Raise();

    }

   
}
