﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName ="Variables", menuName = "Variables/SceneSet Variable", order = 2)]

public class SceneSetVariable : GameEvent {

	public SceneSet Value;

    public void SetValue(SceneSet value)
    {
        Value = value;
        Raise();

    }

    public void SetValue(SceneSetVariable value)
    {
        Value = value.Value;
        Raise();

    }

    
}
