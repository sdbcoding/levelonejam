﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class MoveInFromBottom : MonoBehaviour
{
    public Image trans;
    Vector3 orgPosition;
    Vector3 newPosition;
    float ticks;
    Color invisible = new Color(1.0f, 1.0f, 1.0f, 0.0f);
    Color tempColor;
    public UnityEvent response;

    bool isRunning = false;
    private void Start()
    {
        trans.color = invisible;
        orgPosition = trans.transform.position;
        newPosition = orgPosition;
        newPosition.y -= 500;
        trans.transform.position = newPosition;
    }
    public void Move()
    {
        isRunning = true;
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!isRunning) { return; }

        ticks += Time.deltaTime * 0.5f;
        tempColor = trans.color;
        tempColor.a += ticks;
        if (tempColor.a > 1.0f)
        {
            tempColor.a = 1.0f;
        }
        trans.color = tempColor;
        trans.transform.position = Vector3.Lerp(newPosition, orgPosition, ticks);
        if (ticks >= 1.0f)
        {
            isRunning = false;
            response.Invoke();
        }
    }
}
