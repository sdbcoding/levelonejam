My name in this project is Rokoko Suit, as the computer I used ws previous used for that purpose.






A-B Baddecisions Truckdriver
-Fucked up Meter (F): 100>0 (-1F/2secs). Takes 180-360secs (on the road) to complete the game.
-Truck damage (T): 5 (-1/hit) 
-Intro: Main characters speaks while the 'decision screen' is appearing (ideally after a button for 'take the action' is pressed)
-Decision pressed: Decision sound plays. 
-Outro: Main characters speaks while the 'decision screen' is dissapearing and we get back on the road.
---------------------------------------------------------------------------------------------------------------
#001 - Gas Station
Intro: My Truck is thirsty and so am I! 
TEXT: You have arrived to a filthy gas station. Prices are outrageous. 
1) Tank up and drive away without paying (+1T +20F) 
PERSON: Come back you fucker! 
2) Throw beer cans at people (+30F)
PERSON: Hey, you Asshole!  
3) Ligt up a smoke while tanking  (-1T +10F) 
PERSON: Nononono! (fire starting)
Outro: (He-he-he...) 
---------------------------------------------------------------------------------------------------------------
#002 - Brothel
Intro:  Im gonna go get me some! 
You are near by a brothel, where experienced ladies share their love.  It ain't free though. 
1) Sell engine parts for some lovin'! (+40F -2T)
PERSON: Gonna show you some good time, honey! 
2) Charm them with a dirty joke! (0)
PERSON: Get lost asshole! 
3) Fantasise improprietly about the ladies! (+10F) 
PERSON: Why don't you come in, honey? 
Outro: Ooo yeah. 
---------------------------------------------------------------------------------------------------------------
#003 - Hitchhiker
Intro: Wow, this guy is ugly as fuck. 
There is a hitchhiker on the side of the road. 
1) Throw a beer can at him (+20F)
PERSON: Motherfucker! 
2) Take him on a bumpy ride! (-10F)
PERSON: Oh Im feeling sick... Am gonna throw up... Stop the truck! 
3) Try to hit him (-2T)
PERSON: Watch ooouu!! (metal crash sound) 
Outro: Eeeh, fucking hitchhikers man, fuck 'em! 
---------------------------------------------------------------------------------------------------------------
#004 -Vegetable stand 
Intro: Freeesh vegies coming up! 
You can se a lonely vegetable stand. No one is nearby. 
1) Drive into the vegie stand! (+30F)
We-ho! Making some salad over here! 
2) Wait for the merchat to buy some vegies! (0)
Its been two hours - where the hell is he? 
3) Dream about ratatoulie your momma used to make! (+20F)
Those were the times. 
Outro: My momma always used to make me eat allll the vegies. 
---------------------------------------------------------------------------------------------------------------
#005 - Amigos
Intro: Some hombres! 
Bunch of Mexicans are watching the passing cars and chilling. 
1) Have couple of beers with them before you hit the road again! (+2T)
PERSON: Hey Senor, your oil is leaking! Juan will fix that! 
2) Laugh at them because they don't have any beers! (+20F) 
PERSON: Whats your problem? 
3) Give them the finger! (-1T, +30F) 
PERSON: We gonna fuck you up!
Outro:  Aaalways good to make new friends. 
---------------------------------------------------------------------------------------------------------------
#006 - Shopping mall 
Intro: Thats some big-ass store! 
There is a big fancy shopping mall with big fancy parking lot on the side of the road.
1) Start drifting in the parking lot! (-1T +30F)
Ooo yeeee... Lets see what this truck can do! (metal crash)
2) Go shopping for beers on sale!  (0)
What? My credit cards was declined! Oh wait, thats a beer coaster... 
3) Go 'shopping' in the mall's trash containers! (+1T +10F)
Time to stuck up on some supplies!
Outro:  (whistling)
---------------------------------------------------------------------------------------------------------------
#007 - Mechanic shop
Intro: Oh I know this mechanic!  He's a real jerk..
You recognize a mechanic in his repair shop. He used to bully you when you were younger.
1) Ask him nicely to repair your truck. (+3T, -20F)
PERSON: Whats up fatty? Your truck can't hold your weight? Ha-ha-ha! 
2) Ask him angrily to repair your truck. (+1T -10F)
PERSON: Haha! Hey everyone! Look how fatty is trying to act like a big boy! Ha-ha-ha!
3) Punch him in the face! (+30F)
PERSON: Hey what the fuck fatty? 
Outro:  Ye, fuck that guy, man... (beer opens)
---------------------------------------------------------------------------------------------------------------
#008 - Fast food
Intro: Time for a snack!
Big sign lets you know that the fast food restaurant McDolores is nearby.
1) Order a mega-size menu (+20F)
PERSON: Do you want fries with that?
2) Order a giga-size menu (+30F)
PERSON: Do you want chicken with that?
3) Order peta-size menu (but with a diet coke!) (+10F)
PERSON: We don't have diet coke, sorry sir!
Outro:  Nothin' like a proper lunch!
---------------------------------------------------------------------------------------------------------------
#009 - Rest area 
Intro: Maybe I should take a nap.
Empty space on the side of the road gives an ideal opportunity to take a break.
1) Have few beers (+20F)
Ahh.... (beer open sound)
2) Watch the beautifull nature scenery! (0)
Man... it fucking boring here!
3) Fix the truck a little (+1T -10F)
Fuck! Now my hands are dirty!
Outro:  Alright, what else is on the radio?
---------------------------------------------------------------------------------------------------------------
#010 - Billboard
Intro: I need to take a piss. 
You drove by a big billboard inviting everyone for a concert of  mexican rockstar 'Julio Guacamole' to relieve yourself.  
1) Piss on the Bilboard! (+30F)
Hehe... How do you like that Julio?!
2) Piss the road! (+10F)
Ah... 
3) Piss on the truck! (-1T)
Hehehe... Oh shit, I hope the truck will start now!
Outro:  Im gonna need to refil my beer supply!
---------------------------------------------------------------------------------------------------------------
#011 - Wrecked car
Intro: Ooo... someone had one too many!
Someone has crashed his car into a nearby tree. Bottles of tequila are laying all around the place.
1) Stop to laugh at the drunk! (+10F)
PERSON: (drunkenly) Whaaat? I ain't drunk!
2) Offer a consolidating beer. (+30F)
PERSON: (drunkenly) Thaaanks man. Here, lets celebrate with a shot of tequila!
3) Steal some of his tequila bottles! (-1T, +20F)
PERSON: (drunkenly) Hey, stop or I'll shoot! (shoots) 
Outro:  My dad always told me: Son, never drink tequila behind the wheel. Stick to beer. 
---------------------------------------------------------------------------------------------------------------
#012 - Dumping site
Intro: Ooo yeah, this place is like a all-you-can-take buffee. 
Smelly garbage site is nearby, with all kinds of dirty treasures. 
1) Look for some spare parts for the truck! (+2T +10F)
Nice! There is the 4th wheel I was missing!
2) See if you get lucky finding some booze! (0)
Nah, the booze is sold out.
3) Drive through the garbage bags with your truck! (+30F)
Fuck yeeah!
Outro:  And now on the damn road again... 
---------------------------------------------------------------------------------------------------------------
#013 - Speed Cam
Intro: Fuck the police cams!!
Lonely police camera is taking photos of everyone going above the speed limit
1) Drive into it! (-1T +30F)
Take that, technology! 
2) Slow down. (0)
Fuuucking hate this.
3) Speed up. (+20F)
(Satisfied) My boss is not gonna be happy about that. He-he.
Outro:  (annoyed) Are we there yet!?
---------------------------------------------------------------------------------------------------------------
#014 - Biker
Intro: Look at this cocky asshole.
A biker dressed in black leather is overtaking you on the road. 
1) Whistle at him and give him the finger! (-1T, +30F)
PERSON: Oh yeah? Come here! (metal hit)
2) Push him out of the road! (-2T, +20F)
PERSON: The fuck you doing? (metal crash)
3) Tell him what an asshole he is! (+10F)
PERSON: Fuck off, jackass! 
Outro:  Hehe. That showed him whos the boss here!
---------------------------------------------------------------------------------------------------------------
#015 - Truck Parking
Intro: Truck Parking. Closest thing I have to call home. 
You have arrived to a large parking spot with several parked trucks. You recognize some of the drinking truck drivers. 
1) Trade with some spare parts for the truck.(+1T)
PERSON: Ye mate, have these, I don't need them no more. 
2) Challenge someone to a truck race! (-2T, +20F)
PERSON: Looser pays for the beers! Yee-hoo! 
3) Get properly wasted before you keep on going! (-3T, +30F)
PERSON: Hey, maybe you should be driving like that? -Ssshut up!
Outro:  Home sweet home. 
---------------------------------------------------------------------------------------------------------------
#016 - Alcohol shop
Intro: Finally! I need a re-fill! 
You have stopped your truck in front of a 24/7 alcohol shop. It is currently closed. 
1) Break the front door with the truck to get to the booze! (0)
PERSON: Stop! I have a shotgun here! Just back up the truck nice and slow!
2) Honk the horn until someone lets you in! (+20F)
PERSON: Fuck, Im comming, Im comming! Here, come on in. 
3) Cry desperately because there is no booze left! (+30F)
PERSON: Hey, man, what happend? Ahh, don't cry. Here, have this one on me. 
Outro: (beer can opens)
---------------------------------------------------------------------------------------------------------------
#017 - Historical site
Intro:  The fuck is this shit. 
You are nearby a grand historical monument. Posters nearby invite to a fun lecture!
1) Lets see how fun the lecture is. (-10F)
PERSON: So as you see, there are three approaches one can take when studying the interdisciplinary relations of the 18th century engagement of -Fuuuck this is boooring!
2) Lets see how fun the monument is. (-10F)
PERSON: How do you like this marvelous monument sir? -Its booring as FUCK!
3) Lets see how fun getting the fuck out of here is. (0)
PERSON: Wait sir! You will miss our lecture!!
Outro:  Well that was fucking aweful. I need some FUN!
---------------------------------------------------------------------------------------------------------------
#018 - Fire
Intro: Hey... that stuff is burning!
Homeless man is setting some pile of rubish on fire. He seems very protective of the fire. 
1) Pour in some gasoline and see what happens! (-1T, +30F)
PERSON: Yees, Yees! You are making the magic stronger! Hihihihihi!
2) Put the fire out to piss the guy off! (-2T)
PERSON: No! Noooo! You broke my Magic! I will destroy your iron carrige! (metal crash)
3) Offer the guy a beer and watch the fire. (+10F)
PERSON: No. Nooo time for drinking! I have to do my magic! Hihihihi... 
Outro:  What a wierdo. Lucky Im civilized! 
---------------------------------------------------------------------------------------------------------------

